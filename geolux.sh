#!/bin/bash

# My personal workflow with the holux m-241
# $author: lars$


MTKBABEL="mtkbabel -s 38400 -p /dev/ttyUSB0"
GPSBABEL="gpsbabel -t -w -i m241 -f /dev/ttyUSB0 -o gpx -F `date +'%F-%H:%M'`.gpx"


OUTPATH=$1
if [ -z $OUTPATH ]; then
    OUTPATH=$HOME/privat/geo/tracks_holux/
fi


if [ -d $OUTPATH ]; then
    if [ -n $FILES ]; then

	n=$(ls -1  $OUTPATH/tracks*.bin | sed 's/^.*[^0-9]\([0-9]*\)\..*$/\1/'|sort -n|tail -1)
	n=${n##0}
	echo $n
	let n+=1
	echo $n
	OUTPATH=$OUTPATH/tracks${n}
    else
	OUTPATH=$OUTPATH/tracks001
    fi
    #OUTPATH=`mktemp --tmpdir=$OUTPATH track.XXXXX`
elif [ -w $OUTPATH ]; then
nop;
else 
    echo "$OUTPATH not writable"
    exit 1
fi
echo "Writing to file $OUTPATH"
 
$MTKBABEL -l off -f $OUTPATH -a -i -w -t

echo "Written to file $OUTPATH"

# deleting stuff after download?

read -n 1 -p "\nDelete all Tracks/Waypoints on device? [y/N] " INPUT_DELETE

case $INPUT_DELETE in
    ("y" | "Y" ) 
    echo "Delete";
    $MTKBABEL -E;
    ;;
esac
    
