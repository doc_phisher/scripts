#!/bin/bash

# These are some routine state-change scripts to easily switch between monitor setups, network setups etc. They may be triggered by buttons or commandline or other scripts. The current testing environwent is an ArchLinux installation. But some parts might even work on other platforms



##################### Finders


get_wlan_nic() {
	# find lines containing wlp, extract the string until the second colon
	ip addr | sed -n '/wlp/{s/[[:digit:]]*: \(wlp[^:]*\):.*/\1/;p};' | head -n1
}

get_eth_nic() {
	# find lines that only contain enp<number>s<number> before a colon
	# extract the string between the two colons
	ip addr | sed -n '/enp[[:digit:]]\{1,\}s[[:digit:]]\{1,\}:/{s/[[:digit:]]: \(enp[[:digit:]]\{1,\}s[[:digit:]]\{1,\}\):.*/\1/;p}' | head -n1
}


get_usb_nic() {
	# find a string similar to the eth-string, but with an additional letter u and some numbers
	# again: extract the thing between the colons
	ip addr | sed -n '/enp[[:digit:]]s[[:digit:]]\{1,\}u/{s/.*: \(.*\):.*/\1/p };' | head -n1
}

wacom_ids() {
	xsetwacom --list devices | sed 's/^.*id: \([0-9]*\).*$/\1/'
}


## Derive the name of the monitor (given part of the monitor name, which
## will be either VGA or LVDS)
get_monitor() {
	SEARCH=${1:-VGA}
	# xrandr lines directly start with the device name ended by a space (that's convenient)
	xrandr| sed -n "/${SEARCH}/{s/^\(${SEARCH}[^ ]*\) .*/\1/;p};"
}

is_connected_screen(){
	[[ -n `xrandr | grep "${1}.* connected"` ]]
}

get_external_screen(){
for m in $(get_monitor HDMI) $(get_monitor VGA); do
	if (is_connected_screen ${m}) ; then
		echo $(get_monitor ${m}) 
		break
		
	fi
done
return ""
}
###########################################


## find USB-NIC device name and run dhcp on the device
## This is actually the only thing I seem to have to do.
## No delete-function given yet. (Because I am not yet natural with
## ip)
tether_usb () {
	USB=$(get_usb_nic)
	if [ -z "${USB}" ]; then
		sudo dhcpcd $(get_usb_nic)
	else
		echo "USB NIC not found" 1>&2
	fi
}




## Rotates the screen (and wacom mapping if necessary)
## Does only support single screen
rotate_screen() {
	case $1 in
		left)
		wrot=ccw
		xrot=left
		;;
		right)
		wrot=cw
		xrot=right
		;;
		*)
		xrot=normal
		wrot=none
		;;
	esac
	xrandr --output LVDS1 --rotate $xrot
	for i in $(wacom_ids); do 
		xsetwacom --set "$i" rotate $wrot
	done
}


torRunning() {
	[ -n "`systemctl status tor | grep 'active (running)'`" ]
}



EXTERNAL_SCREEN=$(get_external_screen)     # get external monitor name 
LVDS=$(get_monitor "LVDS")   # get internal monitor name

echo "VGA: ${EXTERNAL_SCREEN}, LVDS: ${LVDS}"

case "$1" in
	office)
	# set double monitor
	xrandr --output ${EXTERNAL_SCREEN} --primary --auto --rotate left --output ${LVDS} --auto --left-of ${EXTERNAL_SCREEN}
	${0} tor start
	#sudo route add default gw 141.99.96.254
	#switch_to_lan
# kludge to reset the usb-keybord
	sudo loadkeys -u neo
	setxkbmap de neo
	;;
	mobile)
	xrandr --output ${EXTERNAL_SCREEN} --off --output ${LVDS} --primary --auto
	#switch_to_wlan
	;;
	f012)
	sudo ifconfig enp0s25 141.99.198.66/25
	sudo route add default gw 141.99.198.126
	sudo 'echo "nameserver 8.8.8.8" >> /etc/resolv.conf'
	;;
	beamer)
	xrandr --output ${EXTERNAL_SCREEN} --primary --auto --rotate normal --output ${LVDS} --auto --same-as ${EXTERNAL_SCREEN}
	;;
	beamer2)
	xrandr --output ${LVDS} --primary --auto --rotate normal --output ${EXTERNAL_SCREEN} --right-of ${LVDS}
	;;
	gaming)
	${0} tor stop
	;;
	usbnet)
	tether_usb
	;;
	rotateleft)
	rotate_screen left
	;;
	rotateright)
	rotate_screen right
	;;
	rotatenot)
	rotate_screen none
	;;
	rotate)
	# toggle rotation 
	case $(xsetwacom --get "`wacom_ids| head -n1`" rotate) in
		none)
		rotate_screen right
		;;
		ccw)
		rotate_screen none
		;;
		cw)
		rotate_screen none
		;;
		*)
		rotate_screen none
		;;
	esac
	;;
	tor)
	case $2 in
		down)
		sudo systemctl stop tor
		sudo systemctl stop polipo
		;;
		*) 
		# startup by default
		# start tor, if not running
		if [ -z "`systemctl status tor | grep 'active (running)'`" ];
			then sudo systemctl start tor
		fi
		if [ -z "`systemctl status polipo | grep 'active (running)'`" ];
			then sudo systemctl start polipo
		fi
		;;
	esac
	;;
	*)
	echo "unknown schema $1"
	;;
esac
